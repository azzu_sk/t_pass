//
//  ShowImageViewController.swift
//  T_PASS
//
//  Created by Nan on 12/03/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit

class ShowImageViewController: UIViewController {
    @IBOutlet weak var imageView : UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collebtionBtn: UIBarButtonItem!
    var imgCntrNum = 0
    var imgArray : [String]?
    var path = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        imageView.isHidden = true
        collebtionBtn.tag = 0
        navigationItem.hidesBackButton = true
        imgCntrNum = (UserDefaults.standard.integer(forKey: "imgNameCntr") - 1)
        print(imgCntrNum)
        getImage(imageName: "test\(imgCntrNum).png")
        
        imgArray = loadImagesFromAlbum(folderName: "")
//        imageView.image = UIImage(contentsOfFile: imgArray![0])
//        print(imgArray)
    }

    @IBAction func actionHome(_ sender : Any){
        let navController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "start")
        
        self.present(navController, animated: true, completion: nil)
    }
    @IBAction func actionCollectionVIEW(_ sender: UIBarButtonItem) {
        if(sender.tag == 0){
            collectionViewShow(sender)
        }
        else{
            collectionViewHide(sender)
        }
    }
    
}

extension ShowImageViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print(imgArray?.count as Any)
        return (imgArray?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ShowImageViewCell", for: indexPath) as! ShowImageViewCell
        cell.img.transform = CGAffineTransform(rotationAngle: .pi / 2)
        cell.img.image = UIImage(named: path + imgArray![indexPath.row])
        return cell
    }
    
    
    func getImage(imageName: String){
        let fileManager = FileManager.default
        let imagePath = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(imageName)
        if fileManager.fileExists(atPath: imagePath){
            imageView.transform = CGAffineTransform(rotationAngle: .pi / 2)
            imageView.image = UIImage(contentsOfFile: imagePath)
        }else{
            print("Panic! No Image!")
        }
    }
    
     func loadImagesFromAlbum(folderName:String) -> [String]{
        
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
        let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        var theItems = [String]()
        if let dirPath          = paths.first
        {
            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent(folderName)
            do {
                theItems = try FileManager.default.contentsOfDirectory(atPath: imageURL.path)
//                print(imageURL.path)
                path = imageURL.path
                return theItems
            } catch let error as NSError {
                print(error.localizedDescription)
                return theItems
            }
        }
        return theItems
    }
    func collectionViewShow(_ sender : UIBarButtonItem) {

        UIView.animate(withDuration: 0.3) {
            self.imageView.alpha = 0
            self.collectionView.alpha = 1
        }
        sender.tag = 1
    }
    func collectionViewHide(_ sender : UIBarButtonItem) {
        UIView.animate(withDuration: 0.3, animations: {
            self.collectionView.alpha = 0
            self.imageView.alpha = 1
        }) { (finished) in
            self.collectionView.isHidden = finished
        }
        sender.tag = 0
    }
}
//extension UIImage {
//    func rotate(radians: CGFloat) -> UIImage {
//        let rotatedSize = CGRect(origin: .zero, size: size)
//            .applying(CGAffineTransform(rotationAngle: CGFloat(radians)))
//            .integral.size
//        UIGraphicsBeginImageContext(rotatedSize)
//        if let context = UIGraphicsGetCurrentContext() {
//            let origin = CGPoint(x: rotatedSize.width / 2.0,
//                                 y: rotatedSize.height / 2.0)
//            context.translateBy(x: origin.x, y: origin.y)
//            context.rotate(by: radians)
//            draw(in: CGRect(x: -origin.x, y: -origin.y,
//                            width: size.width, height: size.height))
//            let rotatedImage = UIGraphicsGetImageFromCurrentImageContext()
//            UIGraphicsEndImageContext()
//            print(radians)
//            return rotatedImage ?? self
//        }
//
//        return self
//    }
//}

//rotate 90 degrees
//myImageView.transform = CGAffineTransform(rotationAngle: .pi / 2)

//rotate 180 degrees
//myImageView.transform = CGAffineTransform(rotationAngle: .pi)

//rotate 270 degrees
//myImageView.transform = CGAffineTransform(rotationAngle: .pi * 1.5)
