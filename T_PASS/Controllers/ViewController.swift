//
//  ViewController.swift
//  T_PASS
//
//  Created by Nan on 08/03/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit
import YPImagePicker

class ViewController: UIViewController {

    @IBOutlet weak var profileStory_IMG: UIImageView!
    @IBOutlet weak var profilrStory_NAME: UILabel!
    @IBOutlet weak var storyCollectionView: UICollectionView!
    @IBOutlet weak var contentTableview: UITableView!
    
    let imgPic = UIImagePickerController()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }
    @IBAction func actionCameraLaunch(_ sender: Any) {
        cameraClick()
    }
    @IBAction func actionDirectView(_ sender: Any) {
        self.performSegue(withIdentifier: "DirectViewController", sender: self)
    }
}

extension ViewController:UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContentTableViewCell") as! ContentTableViewCell
        cell.likeUnlikeBtn.addTarget(self, action: #selector(likeBtnHit(_:)), for: .touchUpInside)
        cell.commentBtn.addTarget(self, action: #selector(commentBtnHit(_:)), for: .touchUpInside)
        cell.sendBtn.addTarget(self, action: #selector(sendBtnHit(_:)), for: .touchUpInside)
        cell.bookUnbookBtn.addTarget(self, action: #selector(bookBtnHit(_:)), for: .touchUpInside)
        cell.menuBtn.addTarget(self, action: #selector(menuBtnHit(_:)), for: .touchUpInside)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 15
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FollowerCollectionViewCell", for: indexPath) as! FollowerCollectionViewCell
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 545
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.reloadData()
    }
    //Open Camera
    func cameraClick() {
        if(UIImagePickerController.isSourceTypeAvailable(.camera)){
            imgPic.delegate = self
            imgPic.sourceType = .camera
            imgPic.allowsEditing = false
            self.present(imgPic, animated: true, completion: nil)
        }
//        imgPic.delegate = self
//        imgPic.sourceType = .photoLibrary
//        imgPic.allowsEditing = false
//        self.present(imgPic, animated: true, completion: nil)
    }
    
    //View Image in imaheView
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        showImgInImageView = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        self.performSegue(withIdentifier: "ImageViewController", sender: self)
        imgPic.dismiss(animated: true, completion: nil)
    }
    //Table view Button action
    @objc func likeBtnHit(_ sender : UIButton){
        print("likeBtnHit")
        if(sender.tag == 0){
            sender.setImage(UIImage(named: "like"), for: .normal)
            sender.tag = 1
        }
        else{
            sender.setImage(UIImage(named: "unlike"), for: .normal)
            sender.tag = 0
        }
    }
    
    @objc func commentBtnHit(_ sender : UIButton){
        print("commentBtnHit")
        if(sender.tag == 0){
            
        }
        else{
            
        }
    }
    
    @objc func sendBtnHit(_ sender : UIButton){
        print("sendBtnHit")
        if(sender.tag == 0){
            
        }
        else{
            
        }
    }
    
    @objc func bookBtnHit(_ sender : UIButton){
        print("bookBtnHit")
        if(sender.tag == 0){
            sender.setImage(UIImage(named: "bookmark"), for: .normal)
            sender.tag = 1
        }
        else{
            sender.setImage(UIImage(named: "unbookmark"), for: .normal)
            sender.tag = 0
        }
    }
    
    @objc func menuBtnHit(_ sender : UIButton){
//        let alert = UIAlertController(title: "Did you bring your towel?", message: "It's recommended you bring your towel before continuing.", preferredStyle: .actionSheet)

        let alert = UIAlertController()
        
        let actionReport = UIAlertAction(title: "Report", style: .default, handler: nil)
        actionReport.setValue(UIColor.red, forKey: "titleTextColor")
        
        let actionFewerPosts = UIAlertAction(title: "See Fewer Posts Like This", style: .default, handler: nil)
        actionFewerPosts.setValue(UIColor.red, forKey: "titleTextColor")
        
        alert.addAction(UIAlertAction(title: "Share to facebook", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Share to...", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Copy Link", style: .default, handler: nil))
        alert.addAction(actionReport)
        alert.addAction(actionFewerPosts)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true)
    }
}

