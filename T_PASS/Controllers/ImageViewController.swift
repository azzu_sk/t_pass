//
//  ImageViewController.swift
//  T_PASS
//
//  Created by Nan on 11/03/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit

var showImgInImageView : UIImage!
class ImageViewController: UIViewController {

    @IBOutlet weak var capturedPhotoView: UIImageView!
    var imgNameCntr = 0
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        capturedPhotoView.image = showImgInImageView
        if(UserDefaults.standard.value(forKeyPath: "imgNameCntr") != nil){
            imgNameCntr = UserDefaults.standard.integer(forKey: "imgNameCntr")
        }
    }
    @IBAction func actionSaveImage(_ sender: Any) {
        saveIMG(imgName: "test\(imgNameCntr).png")
    }
}

extension ImageViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func saveIMG(imgName : String) {
        UserDefaults.standard.removeObject(forKey: "imgNameCntr")
        let fileMNG = FileManager.default //1 instance of file manager
        let imgPATH = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(imgName) //2 get path of image
        let imgCAPTURED = capturedPhotoView.image //3 get image we took with camera
        let pngDATA = imgCAPTURED!.pngData() //4 get image in png formate
        print(pngDATA as Any)
        let fileSave = fileMNG.createFile(atPath: imgPATH as String, contents: pngDATA, attributes: nil) //5 store the image in document directory
        imgNameCntr += 1 // incrementing imgNameCntr for getting uniq name ever time we save a image
        if(fileSave){
            UIImageWriteToSavedPhotosAlbum(imgCAPTURED!, nil, nil, nil)
            UserDefaults.standard.set(imgNameCntr, forKey: "imgNameCntr")
            self.performSegue(withIdentifier: "ShowImageViewController", sender: self)
        }else{
//            print("Not saved")
        }
    }
}
