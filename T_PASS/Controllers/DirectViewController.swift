//
//  DirectViewController.swift
//  T_PASS
//
//  Created by Nan on 10/03/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit

class DirectViewController: UIViewController {

    @IBOutlet weak var directMsgTableView: UITableView!
    
    var directTableSearch = UISearchController()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        directTableSearch = ({
            let searchController = UISearchController(searchResultsController: nil)
            searchController.searchResultsUpdater = self
            searchController.dimsBackgroundDuringPresentation = false
            searchController.searchBar.sizeToFit()
            searchController.searchBar.showsCancelButton = true
            searchController.searchBar.barTintColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
            directMsgTableView.tableHeaderView = searchController.searchBar
            return searchController
        })()
    }
    @IBAction func actionDirectAdd(_ sender: Any) {
        print("Add Btn Cliked")
    }
}

extension DirectViewController : UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DirectTableViewCell") as! DirectTableViewCell
        cell.userName.text = "Exp"
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        let searchPredicate = NSPredicate(format: "SELF CONTAINS[c] %@", searchController.searchBar.text!)
        print(searchPredicate)
    }
}
