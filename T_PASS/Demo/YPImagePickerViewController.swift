//
//  YPImagePickerViewController.swift
//  T_PASS
//
//  Created by Nan on 11/03/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit
import YPImagePicker

class YPImagePickerViewController: UIViewController {

    let imgPic = UIImagePickerController()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        cameraClick()
//        fireAction()
        let picker = YPImagePicker()
        picker.didFinishPicking { [unowned picker] items, _ in
            if let photo = items.singlePhoto {
                print(photo.fromCamera) // Image source (camera or library)
                print(photo.image) // Final image selected by the user
                print(photo.originalImage) // original image selected by the user, unfiltered
                print(photo.modifiedImage) // Transformed image, can be nil
                print(photo.exifMeta) // Print exif meta data of original image.
            }
            picker.dismiss(animated: true, completion: nil)
        }
        present(picker, animated: true, completion: nil)
    }
    
    func fireAction() {
//        let imgPic = YPImagePicker()
//        imgPic.didFinishPicking { [unowned picker] items, _ in
//            if let photo = items.singlePhoto {
//                print(photo.fromCamera) // Image source (camera or library)
//                print(photo.image) // Final image selected by the user
//                print(photo.originalImage) // original image selected by the user, unfiltered
//                print(photo.modifiedImage) // Transformed image, can be nil
//                print(photo.exifMeta) // Print exif meta data of original image.
//            }
//            picker.dismiss(animated: true, completion: nil)
//        }
//        present(picker, animated: true, completion: nil)
    }
    
    func cameraClick() {
        if(UIImagePickerController.isSourceTypeAvailable(.camera)){
            imgPic.delegate = self as! UIImagePickerControllerDelegate & UINavigationControllerDelegate
            imgPic.sourceType = .camera
            imgPic.allowsEditing = false
            self.present(imgPic, animated: true, completion: nil)
        }
    }
}
