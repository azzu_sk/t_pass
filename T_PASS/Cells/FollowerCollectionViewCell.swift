//
//  FollowerCollectionViewCell.swift
//  T_PASS
//
//  Created by Nan on 08/03/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit

class FollowerCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var fImgCell : UIImageView!
    @IBOutlet weak var fLblNAme : UILabel!
}
