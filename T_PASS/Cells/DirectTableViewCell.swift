//
//  DirectTableViewCell.swift
//  T_PASS
//
//  Created by Nan on 10/03/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit

class DirectTableViewCell: UITableViewCell {
    
    @IBOutlet weak var userImg : UIImageView!
    @IBOutlet weak var userName : UILabel!
    @IBOutlet weak var userLastMsg : UILabel!
    @IBOutlet weak var cameraBtn : UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
