//
//  ShowImageViewCell.swift
//  T_PASS
//
//  Created by Nan on 12/03/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit

class ShowImageViewCell: UICollectionViewCell {
    
    @IBOutlet weak var img : UIImageView!
    
}
